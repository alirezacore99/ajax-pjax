<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "penelitian_dosen".
 *
 * @property int $id
 * @property int $dosen_id
 * @property string $judul
 * @property string $mulai
 * @property string $akhir
 * @property string $tahun_ajaran
 * @property string|null $tim_riset
 * @property int $bidang_ilmu_id
 *
 * @property BidangIlmu $dosen
 * @property BidangIlmu $bidangIlmu
 */
class PenelitianDosen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'penelitian_dosen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dosen_id', 'judul', 'mulai', 'akhir', 'tahun_ajaran', 'bidang_ilmu_id'], 'required'],
            [['dosen_id', 'bidang_ilmu_id'], 'integer'],
            [['judul'], 'string'],
            [['mulai', 'akhir'], 'safe'],
            [['tahun_ajaran'], 'string', 'max' => 5],
            [['tim_riset'], 'string', 'max' => 45],
            [['dosen_id'], 'exist', 'skipOnError' => true, 'targetClass' => BidangIlmu::className(), 'targetAttribute' => ['dosen_id' => 'id']],
            [['bidang_ilmu_id'], 'exist', 'skipOnError' => true, 'targetClass' => BidangIlmu::className(), 'targetAttribute' => ['bidang_ilmu_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dosen_id' => 'Dosen ID',
            'judul' => 'Judul',
            'mulai' => 'Mulai',
            'akhir' => 'Akhir',
            'tahun_ajaran' => 'Tahun Ajaran',
            'tim_riset' => 'Tim Riset',
            'bidang_ilmu_id' => 'Bidang Ilmu ID',
        ];
    }

    /**
     * Gets query for [[Dosen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDosen()
    {
        return $this->hasOne(BidangIlmu::className(), ['id' => 'dosen_id']);
    }

    /**
     * Gets query for [[BidangIlmu]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBidangIlmu()
    {
        return $this->hasOne(BidangIlmu::className(), ['id' => 'bidang_ilmu_id']);
    }
}
